import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet } from 'react-native';

function Signup() {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
    username: ''
  });

  function signupSubmit() {
    fetch('http://localhost:3000/users', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formData)
    })
      .then(res => res.json())
      .then(data => console.log(data))
      .catch(error => console.error(error));
  }

  function signupChange(name, value) {
    setFormData(prevData => ({ ...prevData, [name]: value }));
  }

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Preencha o cadastro abaixo.</Text>
      <TextInput
        style={styles.input}
        placeholder="Nome de Usuário"
        value={formData.username}
        onChangeText={value => signupChange('username', value)}
      />
      <TextInput
        style={styles.input}
        placeholder="Email"
        value={formData.email}
        onChangeText={value => signupChange('email', value)}
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        secureTextEntry
        value={formData.password}
        onChangeText={value => signupChange('password', value)}
      />
      <Button title="Registrar Conta" onPress={signupSubmit} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  formContainer: {
    width: '80%',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 10,
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
});

export default Signup;