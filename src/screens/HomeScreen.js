import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

function HomeScreen({navigation}) {
  return (
      <View>
        <Button
            title='Registre uma Conta'
            onPress={() => navigation.navigate('Signup')}/>
        <Button
            title='Fazer Login'
            onPress={() => navigation.navigate('Login')}/>
      </View>
  );
}

const styles=StyleSheet.create({

});

export default HomeScreen;
