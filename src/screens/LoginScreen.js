import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Login from '../components/Login';

const LoginScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Fazer Login</Text>
      <Login />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});

export default LoginScreen;