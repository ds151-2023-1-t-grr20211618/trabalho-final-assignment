import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

function TaskListScreen() {
  const [listInput, setListInput] = useState('');
  const [taskInput, setTaskInput] = useState('');
  const [lists, setLists] = useState([]);
  const [editListIndex, setEditListIndex] = useState(-1);

  const navigation = useNavigation();

  const addList = () => {
    if (listInput.trim() !== '') {
      const newList = { title: listInput, tasks: [] };

      setLists([...lists, newList]);
      setListInput('');
    }
  };

  const removeList = (index) => {
    const updatedLists = [...lists];
    updatedLists.splice(index, 1);
    setLists(updatedLists);
  };

  const addTask = (listIndex) => {
    if (taskInput.trim() !== '') {
      const newTask = { name: taskInput, completed: false };

      const updatedLists = [...lists];
      updatedLists[listIndex].tasks.unshift(newTask);

      setLists(updatedLists);
      setTaskInput('');
    }
  };

  const toggleTask = (listIndex, taskIndex) => {
    const updatedLists = [...lists];
    const task = updatedLists[listIndex].tasks.splice(taskIndex, 1)[0];
    task.completed = !task.completed;
    if (task.completed) {
      updatedLists[listIndex].tasks.push(task);
    } else {
      updatedLists[listIndex].tasks.unshift(task);
    }
    setLists(updatedLists);
  };

  const editListTitle = (index, newTitle) => {
    const updatedLists = [...lists];
    updatedLists[index].title = newTitle;
    setLists(updatedLists);
    setEditListIndex(-1);
  };

  const editTaskName = (listIndex, taskIndex, newName) => {
    const updatedLists = [...lists];
    updatedLists[listIndex].tasks[taskIndex].name = newName;
    setLists(updatedLists);
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Digite o título da lista"
        value={listInput}
        onChangeText={(text) => setListInput(text)}
        onSubmitEditing={addList}
      />

      {lists.map((list, listIndex) => (
        <View key={listIndex}>
          {editListIndex === listIndex ? (
            <TextInput
              style={styles.input}
              value={list.title}
              onChangeText={(text) => setLists(lists.map((item, idx) => (idx === listIndex ? { ...item, title: text } : item)))}
              onBlur={() => setEditListIndex(-1)}
              onSubmitEditing={() => editListTitle(listIndex, list.title)}
            />
          ) : (
            <TouchableOpacity onPress={() => setEditListIndex(listIndex)}>
              <Text style={styles.listTitle}>{list.title}</Text>
            </TouchableOpacity>
          )}

          <TextInput
            style={styles.input}
            placeholder="Digite uma tarefa"
            value={taskInput}
            onChangeText={(text) => setTaskInput(text)}
            onSubmitEditing={() => addTask(listIndex)}
          />

          {list.tasks.map((task, taskIndex) => (
            <TouchableOpacity
              key={taskIndex}
              onPress={() => toggleTask(listIndex, taskIndex)}
              style={[
                styles.task,
                {
                  backgroundColor: task.completed ? 'gray' : 'yellow',
                  textDecorationLine: task.completed ? 'line-through' : 'none',
                },
              ]}
            >
              <TextInput
                style={styles.taskName}
                value={task.name}
                onChangeText={(text) => editTaskName(listIndex, taskIndex, text)}
              />
            </TouchableOpacity>
          ))}

          <TouchableOpacity onPress={() => removeList(listIndex)} style={styles.removeButton}>
            <Text style={styles.removeButtonText}>Remover Lista</Text>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: 'gray',
    marginBottom: 16,
    paddingHorizontal: 8,
  },
  listTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  task: {
    paddingVertical: 8,
    marginBottom: 8,
  },
  taskName: {
    flex: 1,
  },
  removeButton: {
    backgroundColor: 'red',
    padding: 8,
    alignSelf: 'flex-start',
  },
  removeButtonText: {
    color: 'white',
  },
});

export default TaskListScreen;
