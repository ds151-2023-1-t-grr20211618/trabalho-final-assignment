import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Signup from '../components/Signup';

const SignupScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Registre uma Conta</Text>
      <Signup />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});

export default SignupScreen;